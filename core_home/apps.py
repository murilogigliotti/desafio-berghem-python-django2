from django.apps import AppConfig


class CoreHomeConfig(AppConfig):
    name = 'core_home'
