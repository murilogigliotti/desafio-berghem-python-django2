# DJANGO REST
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated, IsAdminUser, IsAuthenticatedOrReadOnly, DjangoModelPermissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.filters import SearchFilter
from rest_framework.viewsets import ModelViewSet
# MODELS
from core_home.models import Cedulas, Moedas, Valores_Cobrado_Pago
# API
from core_home.api.serializers import CedulasSerializer, MoedasSerializer, ValorCobradoPagoSerializer, CalcularTrocoSerializer


# --------------------------------------------------------------------------------
# Usando ViewSets para criar o endpoint e listar todos os tipos de cédulas
# --------------------------------------------------------------------------------
class CedulasListViewSet(ModelViewSet):

    permission_classes = (DjangoModelPermissions,)
    authentication_classes = (TokenAuthentication,)
    queryset = Cedulas.objects.all()
    serializer_class = CedulasSerializer
    filter_fields = ('id','valor_cedula')
    #filter_backends = (SearchFilter,)
    #search_fields = ('id', 'valor_cedula')

# --------------------------------------------------------------------------------


# --------------------------------------------------------------------------------
# Usando ViewSets para criar o endpoint e listar todos os tipos de moedas
# --------------------------------------------------------------------------------
class MoedasListViewSet(ModelViewSet):

    permission_classes = (DjangoModelPermissions,)
    authentication_classes = (TokenAuthentication,)
    queryset = Moedas.objects.all()
    serializer_class = MoedasSerializer
    filter_fields = ('id', 'valor_moeda')
    #filter_backends = (SearchFilter,)
    #search_fields = ('id', 'valor_moeda')

# --------------------------------------------------------------------------------


# --------------------------------------------------------------------------------
# Usando ViewSets para criar o endpoint e listar todos os valores cobrados e pagos
# --------------------------------------------------------------------------------
class ValorCobradoPagoViewSet(ModelViewSet):

    permission_classes = (DjangoModelPermissions,)
    authentication_classes = (TokenAuthentication,)
    queryset = Valores_Cobrado_Pago.objects.all()
    serializer_class = ValorCobradoPagoSerializer
    filter_fields = ('id','valor_cobrado','valor_pago')
    #filter_backends = (SearchFilter,)
    #search_fields = ('id','valor_cobrado','valor_pago')

# --------------------------------------------------------------------------------


# --------------------------------------------------------------------------------
# Usando ViewSets para criar o endpoint e calcular o troco de cada pagamento
# --------------------------------------------------------------------------------
class CalcularTrocoViewSet(ModelViewSet):

    permission_classes = (DjangoModelPermissions,)
    authentication_classes = (TokenAuthentication,)
    queryset = Valores_Cobrado_Pago.objects.all()
    serializer_class = CalcularTrocoSerializer

# --------------------------------------------------------------------------------
