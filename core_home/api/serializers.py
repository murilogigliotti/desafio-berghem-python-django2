import math
#from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer, SerializerMethodField
from core_home.models import Cedulas, Moedas, Valores_Cobrado_Pago


# --------------------------------------------------------------------------------
# Usando Serializer para criar o endpoint Cedulas
# --------------------------------------------------------------------------------
class CedulasSerializer(ModelSerializer):
    class Meta:
        model = Cedulas
        fields = ['id','valor_cedula']

# --------------------------------------------------------------------------------

# --------------------------------------------------------------------------------
# Usando Serializer para criar o endpoint Moedas
# --------------------------------------------------------------------------------
class MoedasSerializer(ModelSerializer):
    class Meta:
        model = Moedas
        fields = ['id','valor_moeda']

# --------------------------------------------------------------------------------

# --------------------------------------------------------------------------------
# Usando Serializer para criar o endpoint Valores-Cobrado-Pago
# --------------------------------------------------------------------------------
class ValorCobradoPagoSerializer(ModelSerializer):
    class Meta:
        model = Valores_Cobrado_Pago
        fields = ['id', 'valor_cobrado', 'valor_pago']

# --------------------------------------------------------------------------------


# --------------------------------------------------------------------------------
# Usando Serializer para criar o endpoint Calcular
# --------------------------------------------------------------------------------
class CalcularTrocoSerializer(ModelSerializer):
    troco = SerializerMethodField('get_troco')
    troco_cedulas = SerializerMethodField('get_troco_cedulas')
    troco_moedas = SerializerMethodField('get_troco_moedas')

    class Meta:
        model = Valores_Cobrado_Pago
        fields = ['id', 'valor_cobrado', 'valor_pago', 'troco', 'troco_cedulas', 'troco_moedas']

    # verificando se o dinheiro é suficiente para o pagamento
    def get_troco(self, obj):
        positivo = 0
        delta = (float(obj.valor_pago) - float(obj.valor_cobrado))
        if delta < positivo:
            return 'Pagamento insuficiente, faltam R$ %s' % (float(obj.valor_cobrado) - float(obj.valor_pago))
        else:
            return '%s' % (float(obj.valor_pago) - float(obj.valor_cobrado))

    # definindo as notas do troco (parte inteira)
    def get_troco_cedulas(self, obj):
        result_fn = ''
        troco = math.trunc(float(obj.valor_pago) - float(obj.valor_cobrado))
        int_troco = math.trunc(troco)
        lista_cedulas = [100.00, 50.00, 10.00, 5.00, 1.00] #inclui manualmente e não utilizei do banco de dados (evolução)

        for x in range(len(lista_cedulas)):
            result_ct = int_troco / lista_cedulas[x]
            if math.trunc(result_ct) >= 1:
                int_troco = int_troco - (lista_cedulas[x] * math.trunc(result_ct))
                result_fn += '%s nota(s) de R$ %s & ' % (math.trunc(result_ct),lista_cedulas[x])

        return '%s' % result_fn

    # definindo as moedas do troco (parte fracionária)
    def get_troco_moedas(self, obj):
        result_fn = ''
        troco = float(obj.valor_pago) - float(obj.valor_cobrado)
        troco_dec = float(troco) - math.trunc(troco)
        lista_moedas = [0.50, 0.10, 0.05, 0.01]  # inclui manualmente e não utilizei do banco de dados (evolução)

        for x in range(len(lista_moedas)):
            result_ct = math.trunc(troco_dec / lista_moedas[x])
            if result_ct >= 1:
                troco_dec -= (lista_moedas[x] * result_ct)
                result_fn += '%s moeda(s) de R$ %s & ' % (math.trunc(result_ct), lista_moedas[x])
                #result_fn += 'moeda(s) de R$ %s & ' % (lista_moedas[x])
        return '%s' % result_fn
# --------------------------------------------------------------------------------
