#from django.db import models
from djongo import models

# Create your models here.
class Cedulas(models.Model):
    valor_cedula = models.CharField(max_length=1000000)

    class Meta:
        verbose_name = 'Cédula'
        verbose_name_plural = 'Cédulas'
        ordering = ['valor_cedula']

    def __str__(self):
        return self.valor_cedula


class Moedas(models.Model):
    valor_moeda = models.CharField(max_length=1000000)

    class Meta:
        verbose_name = 'Moeda'
        verbose_name_plural = 'Moedas'
        ordering = ['valor_moeda']

    def __str__(self):
        return self.valor_moeda


class Valores_Cobrado_Pago(models.Model):
    valor_cobrado = models.CharField(max_length=1000000)
    valor_pago = models.CharField(max_length=1000000)

    class Meta:
        verbose_name = 'Valor_Cobrado_Pago'
        verbose_name_plural = 'Valores_Cobrados_Pagos'
        ordering = ['id']

    def __str__(self):
        return self.valor_cobrado

