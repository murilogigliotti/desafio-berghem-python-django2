# Generated by Django 2.2.6 on 2019-12-24 12:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core_home', '0002_auto_20191224_0953'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cedulas',
            name='valor_cedula',
            field=models.CharField(max_length=1000000),
        ),
        migrations.AlterField(
            model_name='moedas',
            name='valor_moeda',
            field=models.CharField(max_length=1000000),
        ),
    ]
