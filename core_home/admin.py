from django.contrib import admin
from .models import Cedulas, Moedas, Valores_Cobrado_Pago

# Register your models here.
admin.site.register(Cedulas)
admin.site.register(Moedas)
admin.site.register(Valores_Cobrado_Pago)